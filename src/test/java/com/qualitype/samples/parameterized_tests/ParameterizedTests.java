package com.qualitype.samples.parameterized_tests;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.Arrays;
import java.util.Collection;
import java.util.stream.Stream;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Named;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

/**
 * @author Christian.Miethling
 */
@DisplayName("SomeTests")
public final class ParameterizedTests {

    public static Collection<Object[]> data() {
        return Arrays.asList(new Object[][] {
                // Named.of() see
                // https://github.com/junit-team/junit5/issues/2301#issuecomment-698856868
                { Named.of("a1", true), "true" },
                //
                { Named.of("b1", true), "false" }, });
    }

    // @DisplayNameGeneration(value = "asdf")
    @DisplayName(value = "This name doesn't show in Maven!?")
    @ParameterizedTest(name = "{index}: {0}")
    @MethodSource("data")
    public void methodSourceTest(final boolean bool1, final String bool2) {
        assertEquals(bool1, Boolean.parseBoolean(bool2), bool2 + " is not equal to " + bool1);
    }

    public static Stream<Arguments> data2() {
        return Stream.of(Arguments.of(Named.of("a2", true), "true"), Arguments.of(Named.of("b2", false), "false"));
    }

    @ParameterizedTest(name = "{index}: {0}")
    @MethodSource("data2")
    public void methodSourceTest2(final boolean bool1, final String bool2) {
        assertEquals(bool1, Boolean.parseBoolean(bool2), bool2 + " is not equal to " + bool1);
    }

    @DisplayName("73$71 ✔")
    @Test
    public void name() throws NoSuchMethodException, SecurityException {
        System.out.println(getClass().getDeclaredMethod("name").getAnnotation(DisplayName.class).value());
    }
}
